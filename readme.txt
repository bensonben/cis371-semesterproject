This is the semester long project for CIS371.
In order, to fully use this project you'll have to have "node.js" installed on 
your computer. There are guilds out there to do this.

Then, I recommend you get a terminal bash like tool on windows either I use 
cmder. Google it, and install it.

Ok, so you've Node, and commander installed on your computer. If you're on mac
then you skip the commander portion on this short guide.

Finally, you'll need to globally install "gulp" a tool used for web development.
Some people use grunt instead, but let's not fight about that right now.

I believe the command from the terminal is "npm install gulp -g"
Once gulp is installed you'll be able to navigate into the base directory for
the semester project and run the command "gulp watch" which will automatically 
fire up your web browser.

From there the real power of gulp comes at your finger tips. The advantages are
that once you save a JS, php, html, or .css files and SAVE them. the browser
will automatically clear your cache and reload the page.


